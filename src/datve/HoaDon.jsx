import React, { Component } from 'react'
import { connect } from 'react-redux'
import { thanhToanAction } from '../redux/action/DatVeAction'

class HoaDon extends Component {

    renderHangHoaDon = () => {
        return this.props.danhSachGheDaDat.map((hoaDon, index) => {
            return (
                <tr key={index}>
                    <td>{hoaDon.soGhe}</td>
                    <td>{hoaDon.gia}</td>
                </tr>)
        })
    }

    render() {
        return (
            <div className="modal fade " id="hoaDonModal" tabIndex={-1} aria-labelledby="hoaDonModalLabel" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content bg-transparent" style={{
                        border: '2px dashed yellow'
                    }}>
                        <div className="modal-header">
                            <h5 className="modal-title firstChar" id="hoaDonModalLabel">Kiểm tra vé</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" />
                        </div>
                        <div className="modal-body overflow-auto" style={{ height: '500px' }}>
                            <table className='table firstChar'>
                                <thead>
                                    <tr>
                                        <th>Vị trí</th>
                                        <th>Giá</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderHangHoaDon()}
                                </tbody>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button type="button" onClick={() => this.props.thanhToan()} className="btn firstChar" data-bs-dismiss="modal">Thanh toán</button>
                        </div>
                    </div>
                </div>
            </div>

        )
    }
}

const mapStateToProps = state => {
    return {
        danhSachGheDaDat: state.DatVeReducer.danhSachGheDaDat
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        thanhToan: () => {
            dispatch(thanhToanAction())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HoaDon)
