import React, { Component } from 'react';
import { connect } from 'react-redux';
import {themGheAction} from '../redux/action/DatVeAction'

class HangGhe extends Component {

  renderViTriGhe = () => {

    return this.props.danhSachGheDat.danhSachGhe.map((ghe, index) => {
      let indexGhe = this.props.danhSachGheDaDat.findIndex(gheDaDat=> gheDaDat.soGhe === ghe.soGhe),
       duocChon = '',dangChon='',
       disable = false
       if(ghe.daDat){
        disable = true
        duocChon = 'gheDuocChon'
       }
       if(indexGhe  !==-1){
        duocChon = 'gheDangChon'
       }
      return <button key={index} disabled={disable} onClick={()=>this.props.themGhe(ghe)} className={`ghe ${duocChon} ${dangChon}`}>
        {ghe.soGhe}
      </button>
    })

  }

  renderSoGhe = () => {
    return this.props.danhSachGheDat.danhSachGhe.map((ghe, index) => {
      return <button key={index} className='rowNumber'>
        {ghe.soGhe}
      </button>
    })
  }

  renderGhe = () => {
    let { danhSachGheDat } = this.props
    if (danhSachGheDat.hang === '') {
      return <div className='so_ghe'>
        {danhSachGheDat.hang}{this.renderSoGhe()}
      </div>
    }
    else {
      return <div className='hang_ghe firstChar'>
        {danhSachGheDat.hang} {this.renderViTriGhe()}
      </div>
    }
  }


  render() {
    return (
      <div>
        {this.renderGhe()}
      </div >
    )
  }
}

const mapStateToProps = state => {
  return {
    danhSachGheDaDat: state.DatVeReducer.danhSachGheDaDat
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    themGhe: (gheMoi) => {
      dispatch(themGheAction(gheMoi))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HangGhe)
