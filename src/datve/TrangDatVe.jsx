import React, { Component } from 'react'

import HangGhe from './HangGhe'
import ThongTinGhe from './ThongTinGhe'
import './BaiTapBookingTicket.css'

import danhSachGheDat from '../data/danhSachGhe.json'
import HoaDon from './HoaDon'

export default class TrangDatVe extends Component {

  renderHangGhe = () => {
    return danhSachGheDat.map((ghe, index) => {
      return <div key={index}>
        <HangGhe danhSachGheDat={ghe} />
      </div>
    })
  }

  render() {
    return (
      <div className='bookingMovie' style={{
        background: "url(../img/bganime.jpg)",
        backgroundSize: '100%',
        width: '100%',
      }}>
        
        <div className='movie_content p-4' style={{
          background: 'rgba(0,0,0,0.5)',
          backgroundSize: '100%',
          width: '100%',
          height: '100vh'
        }}>
          <div className="container-fluid">
            <div className="row">
              <div className="col-lg-8 px-0">
                <h1 className='text-warning'>ĐẶT VÉ XEM PHIM</h1>
                <div className='screen mx-auto mb-5'></div>
                {this.renderHangGhe()}
              </div>
              <div className="col-lg-4 px-0 pt-lg-0 pt-4">
                <ThongTinGhe />
                <HoaDon/>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}



