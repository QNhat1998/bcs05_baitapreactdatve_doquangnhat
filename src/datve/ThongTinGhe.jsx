import React, { Component } from 'react'
import { connect } from 'react-redux';
import { xoaGheAction } from '../redux/action/DatVeAction';

class ThongTinGhe extends Component {

  renderDanhSachGhe = () => {
    return (
      this.props.danhSachGheDaDat.map((ghe, index) => {
        console.log(ghe)
        return (
          <tr key={index}>
            <td className='text-warning'>{ghe.soGhe}</td>
            <td className='text-warning'>{ghe.gia.toLocaleString()}</td>
            <td><button onClick={() => this.props.xoaGhe(ghe.soGhe)} className='text-danger bg-transparent border-0'>X</button></td>
          </tr>)
      })
    )
  }

  tinhTongTien = () => {
    let tong = 0
    if (this.props.danhSachGheDaDat.length === 0)
      tong = 0
    else {
      for (let ghe of this.props.danhSachGheDaDat)
        tong += ghe.gia
    }

    return <p>
      <span className='firstChar'>Tổng tiền (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ : {tong.toLocaleString()} vnđ</span>
    </p>
  }

  render() {
    return (
      <div>
        <h1 className='text-light'>DANH SÁCH GHẾ VỪA CHỌN</h1>
        <div className='group-btn my-5' style={{
          display: 'flex',
          color: 'white',
          gap: '10px'
        }}>
          <button className='gheDuocChon'></button><span>Ghế được chọn</span>
          <button className='gheDangChon'></button><span>Ghế đang chọn</span>
          <button className='ghe m-0'></button><span>Ghế chưa chọn</span>
        </div>
        <div className='overflow-auto' style={{
          height: '400px'
        }}>
          <table className='table text-warning text-align'>
            <thead>
              <tr>
                <th>Số ghế</th>
                <th>Giá (vnđ)</th>
                <th>Hủy</th>
              </tr>
            </thead>
            <tbody>
              {this.renderDanhSachGhe()}
            </tbody>
          </table>
        </div>
        {this.tinhTongTien()}
        <button className='firstChar btn' data-bs-toggle="modal" data-bs-target="#hoaDonModal">Đặt chỗ ngay</button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    danhSachGheDaDat: state.DatVeReducer.danhSachGheDaDat
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    xoaGhe: (soGhe) => {
      dispatch(xoaGheAction(soGhe))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ThongTinGhe)
