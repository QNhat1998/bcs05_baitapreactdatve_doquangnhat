import { THEM_GHE,XOA_GHE,THANH_TOAN } from "../types/DatVeType";

export const themGheAction = (ghe) =>{  
    console.log(ghe)
    return{
        type:THEM_GHE,
        ghe
    }
}

export const xoaGheAction = (soGhe) =>{
    return{
        type:XOA_GHE,
        soGhe
    }
}

export const thanhToanAction = () =>{
    return{
        type:THANH_TOAN
    }
}