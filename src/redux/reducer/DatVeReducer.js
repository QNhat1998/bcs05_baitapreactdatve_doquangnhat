import {THEM_GHE,XOA_GHE,THANH_TOAN} from '../types/DatVeType'


const stateGhe = {
    danhSachGheDaDat: [
    ] //Giá trị ban đầu của ghế
}

console.log(stateGhe)
export const DatVeReducer = (state = stateGhe, action) => {
    console.log(state)
    switch(action.type){
        case THEM_GHE:{
            let danhSachGheDangDat = [...state.danhSachGheDaDat];
            let index = danhSachGheDangDat.findIndex(ghe=>ghe.soGhe === action.ghe.soGhe)
            if(index !== -1){
                danhSachGheDangDat.splice(index,1);
            }else{
                danhSachGheDangDat.push(action.ghe)
            }
            state.danhSachGheDaDat = danhSachGheDangDat
            return {...state}
        }
        case XOA_GHE:{
            let danhSachGheDangDat = [...state.danhSachGheDaDat];
            let index = danhSachGheDangDat.findIndex(ghe=>ghe.soGhe === action.soGhe)
            danhSachGheDangDat.splice(index,1)
            state.danhSachGheDaDat = danhSachGheDangDat
            return {...state}
        }
        case THANH_TOAN:{
            let danhSachGheDangDat = [...state.danhSachGheDaDat];
            danhSachGheDangDat.map(ghe=>{
                ghe.daDat = true
            })

            state.danhSachGheDaDat = []
            return {...state}
        }
        default:
            return{...state}
    }
}